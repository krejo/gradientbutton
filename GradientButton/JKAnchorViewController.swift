//
//  ViewController.swift
//  LayoutAnchors
//
//  Created by JOSEPH KERR on 8/15/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit


/**
 View controller to present buttons with layout guides between them
 
 A variable topLayoutConstraint is modified for spacinf from the top
 
 Two variables spacingConstraint1 and spacingConstraint2 is used for spacing between
 buttons
 
 Usage:
 
 - Pressing the action button will alter constraints and animate the changes
 - Pressing the action button again will alter again
 - Pressing the action again will alter the constraints so that the buttons are pinned to the
 bottom
 
 */

class JKAnchorViewController: UIViewController {
    
    var mode = 0
    
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    let someButton: UIButton = UIButton(type: .Custom)
    
    var layoutSpace1: UILayoutGuide?
    var layoutSpace2: UILayoutGuide?
    var layoutSpace3: UILayoutGuide?

    var topLayoutConstraint: NSLayoutConstraint?
    var spacingConstraint1: NSLayoutConstraint?
    var spacingConstraint2: NSLayoutConstraint?

    var equalSpacing = true
    var origColor: UIColor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        origColor = self.view.backgroundColor
        
        if let navC = self.navigationController {
            someButton.setTitle(navC.toolbarHidden ?  "Show Toolbar" : "Hide Toolbar" , forState: .Normal)
        } else {
            someButton.setTitle("SomeButton", forState: .Normal)
        }
        someButton.backgroundColor = UIColor.lightGrayColor()
        someButton.translatesAutoresizingMaskIntoConstraints = false
        someButton.frame = CGRect(x:0, y:0,width: 80,height: 30)
        someButton.addTarget(self, action: #selector(someAction(_:)), forControlEvents: .TouchUpInside)
        view.addSubview(someButton)

        someButton.grayNormal(2.0)
        actionButton.purpleNormal(2.0)
        saveButton.greenNormal(2.0)
        cancelButton.redNormal(2.0)


        cancelButton.setTitle(" CancelButton ", forState: .Normal)
        
        let space1 = UILayoutGuide()
        let space2 = UILayoutGuide()
        let space3 = UILayoutGuide()
        
        view.addLayoutGuide(space1)
        view.addLayoutGuide(space2)
        view.addLayoutGuide(space3)

        let margins = view.layoutMarginsGuide

        /*
         
         edge-|[ space ][SaveButton][ space ][CancelButton][ space ]|-edge
         
         */

        // Leading Space
        margins.leadingAnchor.constraintEqualToAnchor(space1.leadingAnchor).active = true
        space1.trailingAnchor.constraintEqualToAnchor(saveButton.leadingAnchor).active = true
        // Middle Space
        saveButton.trailingAnchor.constraintEqualToAnchor(space2.leadingAnchor).active = true
        space2.trailingAnchor.constraintEqualToAnchor(cancelButton.leadingAnchor).active = true
        // Trailing Space
        cancelButton.trailingAnchor.constraintEqualToAnchor(space3.leadingAnchor).active = true
        space3.trailingAnchor.constraintEqualToAnchor(margins.trailingAnchor).active = true

        saveButton.widthAnchor.constraintEqualToAnchor(cancelButton.widthAnchor).active = true

        // Equal spacing between or not
        if equalSpacing {
            spacingConstraint1 = space1.widthAnchor.constraintEqualToAnchor(space2.widthAnchor)
            spacingConstraint2 = space1.widthAnchor.constraintEqualToAnchor(space3.widthAnchor)
        } else {
            // Leading and trailing are equal  space1 = space3
            // Middle space2 is percentage of space1
            spacingConstraint1 = space1.widthAnchor.constraintEqualToAnchor(space3.widthAnchor)
            spacingConstraint2 = space2.widthAnchor.constraintEqualToAnchor(space1.widthAnchor, multiplier: 0.65)
        }
        spacingConstraint1?.active = true
        spacingConstraint2?.active = true

        // Set the topAnchor of space1
        topLayoutConstraint = space1.topAnchor.constraintEqualToAnchor(topLayoutGuide.bottomAnchor, constant: 120.0)
        topLayoutConstraint?.active = true
        // All other topAnchor spacing is based on space1
        space2.topAnchor.constraintEqualToAnchor(space1.topAnchor).active = true
        space3.topAnchor.constraintEqualToAnchor(space1.topAnchor).active = true
        saveButton.topAnchor.constraintEqualToAnchor(space1.topAnchor).active = true
        cancelButton.topAnchor.constraintEqualToAnchor(space1.topAnchor).active = true

        // Keep a reference to the spaces
        layoutSpace1 = space1
        layoutSpace2 = space2
        layoutSpace3 = space3
        
        // Setup the action button
        actionButton.leadingAnchor.constraintEqualToAnchor(margins.leadingAnchor).active = true
        actionButton.trailingAnchor.constraintEqualToAnchor(margins.trailingAnchor).active = true
        actionButton.topAnchor.constraintEqualToAnchor(topLayoutGuide.bottomAnchor, constant: 30.0).active = true

        // Setup the some button
        someButton.leadingAnchor.constraintEqualToAnchor(margins.leadingAnchor).active = true
        someButton.trailingAnchor.constraintEqualToAnchor(margins.trailingAnchor).active = true
        someButton.topAnchor.constraintEqualToAnchor(topLayoutGuide.bottomAnchor, constant: 80.0).active = true
        
        mode = 1
        
        actionButton.setTitle("Action Mode \(mode)", forState: .Normal)

    }
    
    @IBAction func onAction(sender: UIButton) {

        guard let space1 = layoutSpace1,
            space2 = layoutSpace2,
            space3 = layoutSpace3
            else{
                return
        }

        var bgColor = UIColor.whiteColor()
        
        if mode == 0 {

            // Start up

            if let oColor = origColor {
                bgColor = oColor
            }
            topLayoutConstraint?.active = false
            topLayoutConstraint = space1.topAnchor.constraintEqualToAnchor(topLayoutGuide.bottomAnchor, constant: 120.0)
            topLayoutConstraint?.active = true

            
            spacingConstraint1?.active = false
            spacingConstraint2?.active = false

            if equalSpacing {
                spacingConstraint1 = space1.widthAnchor.constraintEqualToAnchor(space2.widthAnchor)
                spacingConstraint2 = space1.widthAnchor.constraintEqualToAnchor(space3.widthAnchor)
            } else {
                spacingConstraint1 = space1.widthAnchor.constraintEqualToAnchor(space3.widthAnchor)
                spacingConstraint2 = space2.widthAnchor.constraintEqualToAnchor(space1.widthAnchor, multiplier: 0.65)
            }
            
            spacingConstraint1?.active = true
            spacingConstraint2?.active = true
            
        } else if mode == 1 {

            if let oColor = origColor {
                bgColor = oColor
            }

            topLayoutConstraint?.active = false
            topLayoutConstraint = space1.topAnchor.constraintEqualToAnchor(topLayoutGuide.bottomAnchor, constant: 170.0)
            topLayoutConstraint?.active = true
            
            
            spacingConstraint1?.active = false
            spacingConstraint2?.active = false

            spacingConstraint1 = space1.widthAnchor.constraintEqualToAnchor(space3.widthAnchor)
            spacingConstraint2 = space2.widthAnchor.constraintEqualToAnchor(space1.widthAnchor, multiplier: 0.45)
            
            spacingConstraint1?.active = true
            spacingConstraint2?.active = true

        } else if mode == 2 {

            if let oColor = origColor {
                bgColor = oColor
            }
            topLayoutConstraint?.active = false
            topLayoutConstraint = space1.topAnchor.constraintEqualToAnchor(topLayoutGuide.bottomAnchor, constant: 200.0)
            topLayoutConstraint?.active = true
            
            
            spacingConstraint1?.active = false
            spacingConstraint2?.active = false

            spacingConstraint1 = space1.widthAnchor.constraintEqualToAnchor(space3.widthAnchor)
            spacingConstraint2 = space2.widthAnchor.constraintEqualToAnchor(space1.widthAnchor, multiplier: 0.15)
            
            spacingConstraint1?.active = true
            spacingConstraint2?.active = true

        } else if mode == 3 {
            
            bgColor = UIColor.blackColor()
            topLayoutConstraint?.active = false
            let spacing = CGRectGetHeight(saveButton.frame) + 8  // from bottom
            topLayoutConstraint = space1.topAnchor.constraintEqualToAnchor(bottomLayoutGuide.topAnchor, constant: -spacing)
            topLayoutConstraint?.active = true
            
        } else {
            bgColor = UIColor.whiteColor()
        }

    
        UIView.animateWithDuration(0.5, delay: 0, options: [.CurveEaseOut], animations: {
            self.view.layoutIfNeeded()
            self.view.backgroundColor = bgColor
            }, completion: nil)

        mode += 1
        
        if mode > 3 {
            mode = 0
        }
        
        sender.setTitle("Action Mode \(mode)", forState: .Normal)
        
    }

    
    func someAction(sender: UIButton) {
        
        if let navC = self.navigationController {
            someButton.setTitle(navC.toolbarHidden ? "Hide Toolbar" : "Show Toolbar" , forState: .Normal)
            
            navC.setToolbarHidden(!navC.toolbarHidden, animated: true)
        }
    }


}


//        space1.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor).active = true
//        space2.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor).active = true
//        space3.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor).active = true
//        saveButton.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor).active = true
//        cancelButton.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor).active = true


