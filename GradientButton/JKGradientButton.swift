//
//  JKGradientButton.swift
//  GradientButton
//
//  Created by JOSEPH KERR on 7/21/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit
import QuartzCore


enum ColorHelper: Int {
    case buttonGradientGrayStart = 0xFEFEFE
    case buttonGradientGrayEnd = 0xBCBCBC
    case buttonGradientGrayPressedStart = 0xA0A0A0
    case buttonGradientGrayPressedEnd = 0xD8D8D8
}


extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
    //    var color = UIColor(red: 0xFF, green: 0xFF, blue: 0xFF)
    //    var color2 = UIColor(netHex:0xFFFFFF)
}

extension UIColor {
    
    static func buttonTextWhiteColor() -> UIColor             { return UIColor(netHex:0xFFFFFF) }
    static func buttonTextGrayInactive() -> UIColor           { return UIColor(netHex:0xB4B4B4) }
    static func buttonTextGrayActive() -> UIColor             { return UIColor(netHex:0x444444) }
    // Green
    static func buttonGradientGreenStart() -> UIColor         { return UIColor(netHex:0x8FDB0D) }
    static func buttonGradientGreenEnd() -> UIColor           { return UIColor(netHex:0x119E1B) }
    static func buttonGradientGreenPressedStart() -> UIColor  { return UIColor(netHex:0x119E1B) }
    static func buttonGradientGreenPressedEnd() -> UIColor    { return UIColor(netHex:0x90DC0D) }
    // Gray
    static func buttonGradientGrayStart() -> UIColor          { return UIColor(netHex:0xFEFEFE) }
    static func buttonGradientGrayEnd() -> UIColor            { return UIColor(netHex:0xBCBCBC) }
    static func buttonGradientGrayPressedStart() -> UIColor   { return UIColor(netHex:0xA0A0A0) }
    static func buttonGradientGrayPressedEnd() -> UIColor     { return UIColor(netHex:0xD8D8D8) }
    // Red
    static func buttonGradientRedStart() -> UIColor           { return UIColor(netHex:0xEF3131) }
    static func buttonGradientRedEnd() -> UIColor             { return UIColor(netHex:0xB21D1D) }
    static func buttonGradientRedPressedStart() -> UIColor    { return UIColor(netHex:0x971919) }
    static func buttonGradientRedPressedEnd() -> UIColor      { return UIColor(netHex:0xCC2A2A) }
    // Purplish
    static func buttonGradientPurpleStart() -> UIColor        { return UIColor(netHex:0x9D72BB) }
    static func buttonGradientPurpleEnd() -> UIColor          { return UIColor(netHex:0x805D98) }
    static func buttonGradientPurplePressedStart() -> UIColor { return UIColor(netHex:0x805D98) }
    static func buttonGradientPurplePressedEnd() -> UIColor   { return UIColor(netHex:0x9D72BB) }
    // Tan - dark text
    static func buttonGradientTanStart() -> UIColor           { return UIColor(netHex:0xEEE6D3) }
    static func buttonGradientTanEnd() -> UIColor             { return UIColor(netHex:0xE4DCC9) }
    static func buttonGradientTanPressedStart() -> UIColor    { return UIColor(netHex:0xD8D0BE) }
    static func buttonGradientTanPressedEnd() -> UIColor      { return UIColor(netHex:0xE1D9C7) }
    // Sample
    static func buttonGradientSampleStart() -> UIColor        { return UIColor(netHex:0xEEE6D3) }
    static func buttonGradientSampleEnd() -> UIColor          { return UIColor(netHex:0xE4DCC9) }
    static func buttonGradientSamplePressedStart() -> UIColor { return UIColor(netHex:0xD8D0BE) }
    static func buttonGradientSamplePressedEnd() -> UIColor   { return UIColor(netHex:0xE1D9C7) }
    // Border
    static func buttonGradientGreenBorder() -> UIColor        { return UIColor(netHex:0x3D920D) }
    static func buttonGradientGrayBorder() -> UIColor         { return UIColor(netHex:0x979797) }
    static func buttonGradientRedBorder() -> UIColor          { return UIColor(netHex:0xA01D1D) }
}


extension UIButton {
    
    func standardStyle() {
        self.titleLabel?.shadowOffset = CGSize(width: 0.5,height: 0.5)
        self.titleLabel?.font = UIFont.boldSystemFontOfSize(DEFAULT_TEXTSIZE)
        layer.cornerRadius = DEFAULT_CORNER
        layer.borderWidth = DEFAULT_BORDERWIDTH
    }
    
    // MARK: The Public API

    func grayNormal() {
        grayNormal(DEFAULT_CORNER)
        standardStyle()
        layer.borderColor = UIColor.buttonGradientGrayBorder().CGColor
    }
    func grayInactive() {
        grayInactive(DEFAULT_CORNER)
        standardStyle()
        layer.borderColor = UIColor.buttonGradientGrayBorder().CGColor
    }
    func greenNormal() {
        greenNormal(DEFAULT_CORNER)
        standardStyle()
        layer.borderColor = UIColor.buttonGradientGreenBorder().CGColor
    }

    func redNormal() {
        redNormal(DEFAULT_CORNER)
        standardStyle()
        layer.borderColor = UIColor.buttonGradientGreenBorder().CGColor
    }
    
    func greyedOut(grey:Bool){
        self.enabled = !grey
        layer.borderColor = grey ? UIColor.buttonGradientGrayBorder().CGColor : UIColor.buttonGradientGreenBorder().CGColor
    }

    
    // MARK: Detailed setup
    
    func grayNormal(cornerRadius:CGFloat) {
        
        // Set the Disabled gradient. Currently, only Gray is supported
        
        backgroundColor = UIColor.clearColor()
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        
        applyGradient(cornerRadius,
                      fromColor:UIColor.buttonGradientGrayStart(),
                      toColor:UIColor.buttonGradientGrayEnd(),
                      fromColorPressed:UIColor.whiteColor(),
                      toColorPressed:UIColor.whiteColor(),
                      disabled:true)
        
        setTitleColor(UIColor.buttonTextGrayInactive(), forState: .Disabled)
        setTitleShadowColor(UIColor(white: 0.4, alpha: 0.6), forState: .Disabled)
        
        // Set the Normal and Highlighted gradient
        
        applyGradient(cornerRadius,
                      fromColor:UIColor.buttonGradientGrayStart(),
                      toColor:UIColor.buttonGradientGrayEnd(),
                      fromColorPressed:UIColor.buttonGradientGrayPressedStart(),
                      toColorPressed:UIColor.buttonGradientGrayPressedEnd(),
                      disabled:false)
        
        setTitleColor(UIColor.buttonTextGrayActive(), forState: .Normal)
        setTitleColor(UIColor.buttonTextGrayActive(), forState: .Selected)
        setTitleColor(UIColor.buttonTextGrayInactive(), forState: .Highlighted)
        
        setTitleShadowColor(UIColor(white: 0.9, alpha: 0.9), forState: .Normal)
        setTitleShadowColor(UIColor(white: 0.5, alpha: 0.9), forState: .Selected)
        setTitleShadowColor(UIColor(white: 0.5, alpha: 0.9), forState: .Highlighted)
    }
    
    func greenNormal(cornerRadius:CGFloat) {
        
        // Set the Disabled gradient. Currently, only Gray is supported
        
        backgroundColor = UIColor.clearColor()
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        
        applyGradient(cornerRadius,
                      fromColor:UIColor.buttonGradientGrayStart(),
                      toColor:UIColor.buttonGradientGrayEnd(),
                      fromColorPressed:UIColor.whiteColor(),
                      toColorPressed:UIColor.whiteColor(),
                      disabled:true)
        
        setTitleColor(UIColor.buttonTextGrayInactive(), forState: .Disabled)
        setTitleShadowColor(UIColor(white: 0.4, alpha: 0.6), forState: .Disabled)
        
        // Set the Normal and Highlighted gradient
        
        applyGradient(cornerRadius,
                      fromColor:UIColor.buttonGradientGreenStart(),
                      toColor:UIColor.buttonGradientGreenEnd(),
                      fromColorPressed:UIColor.buttonGradientGreenPressedStart(),
                      toColorPressed:UIColor.buttonGradientGreenPressedEnd(),
                      disabled:false)
        
        setTitleColor(UIColor.buttonTextWhiteColor(), forState: .Normal)
        setTitleColor(UIColor.buttonTextWhiteColor(), forState: .Selected)
        setTitleColor(UIColor.buttonTextGrayInactive(), forState: .Highlighted)
        
        setTitleShadowColor(UIColor(white: 0.3, alpha: 0.6), forState: .Normal)
        setTitleShadowColor(UIColor(white: 0.0, alpha: 0.9), forState: .Selected)
        setTitleShadowColor(UIColor(white: 0.0, alpha: 0.9), forState: .Highlighted)
    }
    
    func redNormal(cornerRadius:CGFloat) {
        
        // Set the Disabled gradient. Currently, only Gray is supported
        
        backgroundColor = UIColor.clearColor()
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        
        applyGradient(cornerRadius,
                      fromColor:UIColor.buttonGradientGrayStart(),
                      toColor:UIColor.buttonGradientGrayEnd(),
                      fromColorPressed:UIColor.whiteColor(),
                      toColorPressed:UIColor.whiteColor(),
                      disabled:true)
        
        setTitleColor(UIColor.buttonTextGrayInactive(), forState: .Disabled)
        setTitleShadowColor(UIColor(white: 0.4, alpha: 0.6), forState: .Disabled)
        
        // Set the Normal and Highlighted gradient
        
        applyGradient(cornerRadius,
                      fromColor:UIColor.buttonGradientRedStart(),
                      toColor:UIColor.buttonGradientRedEnd(),
                      fromColorPressed:UIColor.buttonGradientRedPressedStart(),
                      toColorPressed:UIColor.buttonGradientRedPressedEnd(),
                      disabled:false)
        
        setTitleColor(UIColor.buttonTextWhiteColor(), forState: .Normal)
        setTitleColor(UIColor.buttonTextWhiteColor(), forState: .Selected)
        setTitleColor(UIColor.buttonTextGrayInactive(), forState: .Highlighted)
        
        setTitleShadowColor(UIColor(white: 0.3, alpha: 0.9), forState: .Normal)
        setTitleShadowColor(UIColor(white: 0.0, alpha: 0.9), forState: .Selected)
        setTitleShadowColor(UIColor(white: 0.0, alpha: 0.9), forState: .Highlighted)
    }
    
    func sampleNormal(cornerRadius:CGFloat) {
        
        // Set the Disabled gradient. Currently, only Gray is supported
        
        backgroundColor = UIColor.clearColor()
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        
        applyGradient(cornerRadius,
                      fromColor:UIColor.buttonGradientGrayStart(),
                      toColor:UIColor.buttonGradientGrayEnd(),
                      fromColorPressed:UIColor.whiteColor(),
                      toColorPressed:UIColor.whiteColor(),
                      disabled:true)
        
        setTitleColor(UIColor.buttonTextGrayInactive(), forState: .Disabled)
        setTitleShadowColor(UIColor(white: 0.4, alpha: 0.6), forState: .Disabled)
        
        // Set the Normal and Highlighted gradient
        
        applyGradient(cornerRadius,
                      fromColor:UIColor.buttonGradientSampleStart(),
                      toColor:UIColor.buttonGradientSampleEnd(),
                      fromColorPressed:UIColor.buttonGradientSamplePressedStart(),
                      toColorPressed:UIColor.buttonGradientSamplePressedEnd(),
                      disabled:false)
        
        setTitleColor(UIColor.buttonTextGrayActive(), forState: .Normal)
        setTitleColor(UIColor.buttonTextGrayActive(), forState: .Selected)
        setTitleColor(UIColor.buttonTextGrayInactive(), forState: .Highlighted)
        
        setTitleShadowColor(UIColor(white: 0.3, alpha: 0.9), forState: .Normal)
        setTitleShadowColor(UIColor(white: 0.0, alpha: 0.9), forState: .Selected)
        setTitleShadowColor(UIColor(white: 0.0, alpha: 0.9), forState: .Highlighted)
    }
    
    func tanNormal(cornerRadius:CGFloat) {
        
        // Set the Disabled gradient. Currently, only Gray is supported
        
        backgroundColor = UIColor.clearColor()
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        
        applyGradient(cornerRadius,
                      fromColor:UIColor.buttonGradientGrayStart(),
                      toColor:UIColor.buttonGradientGrayEnd(),
                      fromColorPressed:UIColor.whiteColor(),
                      toColorPressed:UIColor.whiteColor(),
                      disabled:true)
        
        setTitleColor(UIColor.buttonTextGrayInactive(), forState: .Disabled)
        setTitleShadowColor(UIColor(white: 0.4, alpha: 0.6), forState: .Disabled)
        
        // Set the Normal and Highlighted gradient
        
        applyGradient(cornerRadius,
                      fromColor:UIColor.buttonGradientTanStart(),
                      toColor:UIColor.buttonGradientTanEnd(),
                      fromColorPressed:UIColor.buttonGradientTanPressedStart(),
                      toColorPressed:UIColor.buttonGradientTanPressedEnd(),
                      disabled:false)
        
        setTitleColor(UIColor.buttonTextGrayActive(), forState: .Normal)
        setTitleColor(UIColor.buttonTextGrayActive(), forState: .Selected)
        setTitleColor(UIColor.buttonTextGrayInactive(), forState: .Highlighted)
        
        setTitleShadowColor(UIColor(white: 0.3, alpha: 0.9), forState: .Normal)
        setTitleShadowColor(UIColor(white: 0.0, alpha: 0.9), forState: .Selected)
        setTitleShadowColor(UIColor(white: 0.0, alpha: 0.9), forState: .Highlighted)
    }
    
    func purpleNormal(cornerRadius:CGFloat) {
        
        // Set the Disabled gradient. Currently, only Gray is supported
        
        backgroundColor = UIColor.clearColor()
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        
        applyGradient(cornerRadius,
                      fromColor:UIColor.buttonGradientGrayStart(),
                      toColor:UIColor.buttonGradientGrayEnd(),
                      fromColorPressed:UIColor.whiteColor(),
                      toColorPressed:UIColor.whiteColor(),
                      disabled:true)
        
        setTitleColor(UIColor.buttonTextGrayInactive(), forState: .Disabled)
        setTitleShadowColor(UIColor(white: 0.4, alpha: 0.6), forState: .Disabled)
        
        // Set the Normal and Highlighted gradient
        
        applyGradient(cornerRadius,
                      fromColor:UIColor.buttonGradientPurpleStart(),
                      toColor:UIColor.buttonGradientPurpleEnd(),
                      fromColorPressed:UIColor.buttonGradientPurplePressedStart(),
                      toColorPressed:UIColor.buttonGradientPurplePressedEnd(),
                      disabled:false)
        
        setTitleColor(UIColor.buttonTextWhiteColor(), forState: .Normal)
        setTitleColor(UIColor.buttonTextWhiteColor(), forState: .Selected)
        setTitleColor(UIColor.buttonTextGrayInactive(), forState: .Highlighted)
        
        setTitleShadowColor(UIColor(white: 0.3, alpha: 0.9), forState: .Normal)
        setTitleShadowColor(UIColor(white: 0.0, alpha: 0.9), forState: .Selected)
        setTitleShadowColor(UIColor(white: 0.0, alpha: 0.9), forState: .Highlighted)
    }
    
    
    func grayInactive(cornerRadius:CGFloat) {
        
        // Set the Disabled gradient
        
        backgroundColor = UIColor.clearColor()
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        
        applyGradient(cornerRadius,
                      fromColor:UIColor.buttonGradientGrayStart(),
                      toColor:UIColor.buttonGradientGrayEnd(),
                      fromColorPressed:UIColor.whiteColor(),
                      toColorPressed:UIColor.whiteColor(),
                      disabled:true)
        
        setTitleColor(UIColor.buttonTextGrayInactive(), forState: .Disabled)
        setTitleShadowColor(UIColor(white: 0.4, alpha: 0.6), forState: .Disabled)
    }
    
    
    private func applyGradient(cornerRadius: CGFloat, fromColor startColor:UIColor,
                               toColor endColor:UIColor, fromColorPressed startColorPressed:UIColor, toColorPressed endColorPressed:UIColor, disabled disabledState:Bool){
        // 10 rounded 5 w 10 rounded
        //let wwidth = cornerRadius*2.0 + 5.0
        
        let wwidth = CGRectGetWidth(self.frame)
        
        // NORMAL STATE
        
        let gradient = CAGradientLayer()
        gradient.frame = CGRectMake(0,0,wwidth ,CGRectGetHeight(self.frame))
        gradient.colors = [startColor.CGColor, endColor.CGColor]
        // start Default value is (0.5,0.0). end Default value is (0.5,1.0).
        var imageBack: UIImage?
        UIGraphicsBeginImageContext(CGSizeMake(wwidth,CGRectGetHeight(self.frame)))
        if let context = UIGraphicsGetCurrentContext() {
            CGContextConcatCTM(context, CGAffineTransformIdentity)
            gradient.renderInContext(context)
            let imageCapture = UIGraphicsGetImageFromCurrentImageContext()
            imageBack = imageCapture.resizableImageWithCapInsets(UIEdgeInsetsMake(cornerRadius, cornerRadius ,cornerRadius ,cornerRadius))
        }
        UIGraphicsEndImageContext()
        
        if disabledState {
            
            self.setBackgroundImage(imageBack, forState:.Disabled)
            
        } else {
            
            self.setBackgroundImage(imageBack, forState:.Normal)
            
            // SELECTED STATE
            
            let gradient2 = CAGradientLayer()
            gradient2.frame = CGRectMake(0,0,wwidth ,CGRectGetHeight(self.frame))
            gradient2.colors = [startColorPressed.CGColor, endColorPressed.CGColor]
            // start Default value is (0.5,0.0). end Default value is (0.5,1.0).
            
            var imageBack2: UIImage?
            
            UIGraphicsBeginImageContext(CGSizeMake(wwidth,CGRectGetHeight(self.frame)))
            if let context2 = UIGraphicsGetCurrentContext() {
                CGContextConcatCTM(context2, CGAffineTransformIdentity)
                gradient2.renderInContext(context2)
                let imageCapture2 = UIGraphicsGetImageFromCurrentImageContext()
                imageBack2 = imageCapture2.resizableImageWithCapInsets(UIEdgeInsetsMake(cornerRadius, cornerRadius ,cornerRadius ,cornerRadius))
            }
            UIGraphicsEndImageContext()
            
            self.setBackgroundImage(imageBack2, forState:.Highlighted)
            self.setBackgroundImage(imageBack2, forState:.Selected)
        }
    }
    
}

private  let DEFAULT_BORDERWIDTH: CGFloat = 1.0
private  let DEFAULT_CORNER: CGFloat = 4.0
private  let DEFAULT_TEXTSIZE: CGFloat = 18.0

