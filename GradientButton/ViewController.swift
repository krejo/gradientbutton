//
//  ViewController.swift
//  GradientButton
//
//  Created by JOSEPH KERR on 7/21/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myButton: UIButton!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    
    var toggle = 1
    
    @IBOutlet weak var topConstraintDemoView: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var middleConstraint: NSLayoutConstraint!
    @IBOutlet weak var centerConstraint: NSLayoutConstraint!
    
    var middleConstraints: [NSLayoutConstraint]?
    var startTopConstraint: CGFloat?
    var startTopConstraintDemoView: CGFloat?
    
    @IBOutlet weak var demoView: DemoView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        demoView.label.text = "View From a Nib with outlets"
        
        startTopConstraint = topConstraint.constant
        startTopConstraintDemoView = topConstraintDemoView.constant
//
//        myButton.greenNormal(2.0)
//        button1.redNormal(4.0)
//        //button2.grayNormal(2.0)
//        button2.sampleNormal(5.0)

        
        myButton.redNormal(2.0)
        button1.purpleNormal(2.0)
        button2.grayNormal(2.0)
        button3.greenNormal(2.0)
        
        //button3.greyedOut(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func actionButton(sender: UIButton) {
        
        if sender == button1 {
            if toggle == 0 {
                middleConstraint.constant = -40
                toggle = 1
            } else {
                middleConstraint.constant = -220
                toggle = 0
            }
            
            UIView.animateWithDuration(1.0, delay: 0, usingSpringWithDamping: 0.70, initialSpringVelocity: 0.2, options: [], animations: {
                self.view.layoutIfNeeded()
                }, completion: {(fini:Bool) in
            })
        }
        else if sender == button2 {
            if toggle == 0 {
                trailingConstraint.constant = 200
                toggle = 1
            } else {
                trailingConstraint.constant = 20
                toggle = 0
            }
            
            UIView.animateWithDuration(1.0, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: [], animations: {
                self.view.layoutIfNeeded()
                }, completion: {(fini:Bool) in
            })
            
        }
        else if sender == myButton {
            if toggle == 0 {
                topConstraintDemoView.constant = startTopConstraintDemoView!
                //topConstraint.constant = startTopConstraint!
                toggle = 1
            } else {
                topConstraintDemoView.constant = startTopConstraintDemoView! + 80
                //topConstraint.constant = startTopConstraint! + 80

                toggle = 0
            }
            
            UIView.animateWithDuration(1.0, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.2, options: [], animations: {
                self.view.layoutIfNeeded()
                }, completion: {(fini:Bool) in
            })
        }
        
    }
    
    
    func centerMiddle(button: UIButton, space:Float) {

        
        let visualFormat = "H:|-(\(space))-[button]-(\(space))-|"
        if let constraints = middleConstraints {
            self.view.removeConstraints(constraints)
        }
        
        middleConstraints =
            NSLayoutConstraint.constraintsWithVisualFormat(visualFormat,
                                                           options: [], metrics: nil, views: ["button":button])
        
        
        
        self.view.addConstraints(middleConstraints!)
        
        UIView.animateWithDuration(1.0, delay: 0.0, options: [], animations: {
            self.view.layoutIfNeeded()
            
            }, completion: {(fini:Bool) in
            })
        
    }

}

