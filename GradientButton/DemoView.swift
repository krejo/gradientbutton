//
//  DemoView.swift
//  GradientButton
//
//  Created by JOSEPH KERR on 7/21/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

import UIKit

class DemoView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var view: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        NSBundle.mainBundle().loadNibNamed("DemoView", owner: self, options: nil)
        
        //self.view.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.autoresizingMask = [.FlexibleHeight,.FlexibleWidth]
        self.view.frame = self.bounds
        self.addSubview(self.view)
        
//        self.addConstraints(
//            NSLayoutConstraint.constraintsWithVisualFormat("H:|-[view]-|", options: [], metrics: nil, views: ["view":self.view])
//        )
//        self.addConstraints(
//            NSLayoutConstraint.constraintsWithVisualFormat("V:|-[view]-|", options: [], metrics: nil, views: ["view":self.view])
//        )

        self.label.text = "Initialzed"
        
    }
}


//self.clipsToBounds = true
//self.view.translatesAutoresizingMaskIntoConstraints = false
//self.view.autoresizingMask = [.FlexibleHeight,.FlexibleWidth]

